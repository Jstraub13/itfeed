desc "Update a feed"
task :update_feed => :environment do
  Feed.self.update_feed!
end

desc "Update all subscribed feeds"
task :update_active_feeds => :environment do
  Feed.self.update_all_feeds
end

desc "Download newest entries"
task :download_newest => :environment do
  Feed.download_newest_entries!
end

