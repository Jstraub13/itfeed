class FavoriteEntriesController < ApplicationController
  before_action :set_entry
  
  def create
    if Favorite.create(favorited: @entry, user: current_user)
      redirect_to @entry, notice: 'Article has been favorited'
    else
      redirect_to @entry, alert: 'Something went wrong...'
    end
  end
  
  def destroy
    Favorite.where(favorited_id: @entry.id, user_id: current_user.id).first.destroy
    redirect_to @entry, notice: 'Entry is no longer in favorites'
  end
  
  private
  
  def set_entry
    @entry = Entry.find(params[:entry_id] || params[:id])
  end
end