class FeedsController < ApplicationController
  before_action :authenticate_user!

  require 'rake'

  def index
    @feeds = Feed.where(user: current_user).order("created_at DESC")
  end

  def show
    @feed = Feed.find(params[:id])
    @entries = @feed.entries.all.order(published: :asc)
  end

  def new
    @feed = Feed.new
  end

  def create
    @feed = Feed.new(feed_params)
    @feed.user_id = current_user.id
    
    respond_to do |format|
      if @feed.save
        format.html { redirect_to feeds_path, notice: 'Subscription was successfully created.' }
        format.json { render action: 'show', status: :created, location: @feed }
      else
        format.html { render action: 'new' }
        format.json { render json: @feed.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @feed = Feed.find(params[:id])
    @feed.update(feed_params)
    redirect_to feed_path(@feed)
  end

  def destroy
     @feed = Feed.find(params[:id])
     @feed.destroy
     respond_to do |format|
     format.html { redirect_to feeds_url, notice: 'Feed was successfully destroyed.' }
     format.json { head :no_content }
   end
  end

def refresh
    call_rake :update_feed, feed_id: params[:id].to_i
    flash[:notice] = 'Updated Feed'
    redirect_to feeds_path
  end

  def download_newest
    call_rake :download_newest, feed_id: params[:id].to_i
    flash[:notice] = 'Finished downloading newest entries for that feed'
    redirect_to feeds_path
  end

  private

  def feed_params
    params.require(:feed).permit(:url)
  end
end
