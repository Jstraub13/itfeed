class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :feeds, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :favorite_entries, through: :favorites, source: :favorited, source_type: 'Entry'

  def entries
    entries = []
    self.feeds.each do |feed|
      feed.entries.each do |entry|
        entries << entry
      end
    end
	entries
  end
 end
