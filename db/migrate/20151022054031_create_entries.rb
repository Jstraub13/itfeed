class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.text :url
      t.text :title
      t.text :author
      t.text :content
      t.datetime :published
      t.integer :feed_id

      t.timestamps null: false
    end
  end
end
