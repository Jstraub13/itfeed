class AddReadToEntries < ActiveRecord::Migration
  def change
    add_column :entries, :read, :boolean, default: false
  end
end
